package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;


public class MinMaxTest {

	@Test
    public void compareFirstBigger() throws Exception {
		assertEquals("First is bigger", 9, new MinMax().compare(9,5));
    }

	@Test
    public void compareFirstSmaller() throws Exception {
		assertEquals("First is bigger", 9, new MinMax().compare(5,9));
    }
	
	@Test
    public void compareEqual() throws Exception {
		assertEquals("First is bigger", 9, new MinMax().compare(9,9));
    }
}
